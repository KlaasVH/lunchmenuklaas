﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization.Configuration;

namespace lunchplanning
{
    class Program
    {
        static string bevestig = "Bevestig uw ingave met een druk op de toets ENTER. ";
        static void Main(string[] args)
        {
            bool exit = false;
            while (!exit)
            {
                exit = Menu();
            }
        }
        static bool Menu()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            bool exit = false;
            string[] menu;
            Console.Clear();
            NieuweLijn(1);
            Console.WriteLine("     ********************************************");
            Console.WriteLine("     *               Welkom!                    *");
            Console.WriteLine("     * Lunch menu creator is klaar voor gebruik *");
            Console.WriteLine("     *  --------------------------------------- *");
            Console.WriteLine("     *            Maak uw keuze                 *");
            Console.WriteLine("     *            -------------                 *");
            Console.WriteLine("     *            1: Nieuw menu                 *");
            Console.WriteLine("     *            2: Oude menu's                *");
            Console.WriteLine("     *            3: Menu verwijderen           *");//Danny
            Console.WriteLine("     *            4: Exit                       *");
            Console.WriteLine("     *                                          *");
            Console.WriteLine("     ********************************************");
            Console.WriteLine($"     *  {bevestig}      ");
            NieuweLijn(1);
            int keuze = Keuzestress(1, 4);//Danny
            switch (keuze)
            {
                case 1:
                    string fileName = Ingave(" * Kies uw filenaam ");
                    fileName = "./" + fileName + ".txt";
                    if (File.Exists(fileName))
                    {
                        File.Delete(fileName);
                    }
                    menu = ResetAlles();
                    using (StreamWriter writer = new StreamWriter(fileName, true))
                    {
                        foreach (string x in menu)
                        {
                            writer.WriteLine(x);
                        }
                    }
                    break;
                case 2:
                    menu = Menumenus();
                    break;
                case 3: //Danny 
                    Console.Clear();
                    Console.WriteLine(" * Volgende menu's zijn aanwezig: ");
                    Console.WriteLine("  ------------------------------- ");
                    Console.WriteLine(" * Welk menu wilt u verwijderen? ");
                    Console.WriteLine("  ------------------------------ ");
                    Console.WriteLine(" * Maak uw keuze ");
                    Console.WriteLine("  -------------- ");
                    Console.WriteLine($" * {bevestig}");
                    NieuweLijn(1);
                    ListFilesInDirectory();
                    Console.WriteLine(" * Druk op eender welke toets om verder te gaan");
                    Console.ReadKey();
                    break;
                case 4:
                    exit = true;
                    break;
                default:
                    break;
            }
            return exit;
        }
        static void ListFilesInDirectory()//Danny
        {
            var filenames = Directory.EnumerateFiles(Directory.GetCurrentDirectory(), "*.txt", SearchOption.TopDirectoryOnly).Select(Path.GetFileName);
            string[] fnames = new string[filenames.Count()];
            int count = 0;
            foreach (string filePath in filenames)
            {
                fnames[count] = filePath;
                count++;
                Console.WriteLine(count + ": " + filePath);
                NieuweLijn(1);
            }
            int keuze = Keuzestress(1, count);
            File.Delete(Directory.GetCurrentDirectory() + @"\" + fnames[keuze - 1]);
            Console.WriteLine(fnames[keuze - 1] + " is verwijderd");
            NieuweLijn(1);
            Console.ForegroundColor = ConsoleColor.Green;
        }
        static string[] Menumenus()
        {
            //zoek de naam van het project (2 directories/mappen terug/naar boven gaan en daarvan de naam pakken)
            string newPath = Path.GetFullPath(Path.Combine("./", @"..\..\"));
            string[] directories = newPath.Split('\\');
            string directory = directories[directories.Length - 2];
            //default is "leeg"
            string input = "leeg";
            //geef alle bestanden en de optie om een nieuwe te maken           
            Console.Clear();
            NieuweLijn(1);
            Console.WriteLine(" * Welk menu wilt u aanpassen? ");
            Console.WriteLine("  ---------------------------- ");
            Console.WriteLine(" * Maak uw keuze ");
            Console.WriteLine("  -------------- ");
            NieuweLijn(1);
            string[] vollelijst = Directory.GetFiles("./");
            int count = 0;
            string[] lijst = new string[vollelijst.Length - 3];
            foreach (string x in vollelijst)
            {
                string temp = "";
                for (int i = 2; i < x.Length - 4; i++)
                {
                    temp += x[i];
                }
                //negeer onnodige bestanden uit de lijst adhv projectnaam (eerste drie letter ervan, dus een naam met minder letters geeft hier error)
                if (temp[0] == directory[0] && temp[1] == directory[1] && temp[2] == directory[2])
                { }
                //voeg alle nodige bestanden toe aan lijst
                else { lijst[count] = temp; count++; }
            }
            count = 0;
            foreach (string x in lijst)
            {
                count++;
                Console.WriteLine("  " + count + " : " + x);
                Console.WriteLine(" ------------------------------");
            }
            count++;
            NieuweLijn(1);
            Console.WriteLine("  " + count + " : Terug naar menu ");
            NieuweLijn(1);
            Console.WriteLine($" * {bevestig}");
            NieuweLijn(1);
            int keuze = Keuzestress(1, count);
            //als je een bestaand bestand kiest
            string[] output = new string[1];
            if (keuze != count)
            {
                input = ("./" + lijst[keuze - 1] + ".txt");
                using (StreamReader reader = new StreamReader(input))
                {
                    int index = File.ReadAllLines(input).Length;
                    output = new string[index];
                    int x = 0;
                    while (reader.Peek() >= 0)
                    {
                        output[x] = reader.ReadLine();
                        x++;
                    }
                }
                Menupermenu(output, input);
            }
            return output;
        }
        static string[] Menupermenu(string[] menu, string fileName)
        {
            NieuweLijn(1);
            Console.WriteLine("     ************************************");
            Console.WriteLine("     *                                  *");
            Console.WriteLine("     *          Maak uw keuze           *");
            Console.WriteLine("     *          -------------           *");
            Console.WriteLine("     *          1: Weergeven            *");
            Console.WriteLine("     *          2: Aanpassen            *");
            Console.WriteLine("     *          3: Terug naar menu      *");
            Console.WriteLine("     *                                  *");
            Console.WriteLine("     ************************************");
            NieuweLijn(1);
            Console.WriteLine($"     *  {bevestig}");
            int keuze = Keuzestress(1, 4);
            switch (keuze)
            {
                case 1:
                    Console.Clear();
                    foreach (string x in menu)
                    {
                        Console.WriteLine(" " + x);
                        Console.WriteLine("  ---------------------------- ");
                    }
                    Menupermenu(menu, fileName);
                    break;
                case 2:
                    menu = Aanpassing(menu);
                    using (StreamWriter writer = new StreamWriter(fileName, false))
                    {
                        foreach (string x in menu)
                        {
                            writer.WriteLine(x);
                        }
                    }
                    Menupermenu(menu, fileName);
                    break;
                case 3:
                    break;
                default:
                    break;
            }
            return menu;
        }
        static string[] Aanpassing(string[] menu)
        {
            Console.Clear();
            int keuzedag = Dagkeuze(menu);
            Console.Clear();
            Console.WriteLine("  " + menu[(keuzedag - 1) * 3]);
            NieuweLijn(1);
            Console.WriteLine(" * 1: " + menu[(keuzedag - 1) * 3 + 1]);
            NieuweLijn(1);
            Console.WriteLine(" * 2: " + menu[(keuzedag - 1) * 3 + 2]);
            NieuweLijn(1);
            Console.WriteLine(" * 3: Kopieer een andere dag");
            NieuweLijn(1);
            Console.WriteLine(" * Welk gerecht wilt u aanpassen? Maak uw keuze. ");
            NieuweLijn(1);
            Console.WriteLine($" * {bevestig}");
            int keuzegerecht = Keuzestress(1, 3);
            Console.Clear();
            if (keuzegerecht != 3)
            {
                NieuweLijn(1);
                Console.WriteLine(" * Oude gerecht: " + menu[(keuzedag - 1) * 3 + keuzegerecht]);
                Console.WriteLine("  -------------------------------------------------------- ");
                NieuweLijn(1);
                menu[(keuzedag - 1) * 3 + keuzegerecht] = Ingave(" * Wat is het nieuwe gerecht? ");
                NieuweLijn(1);
            }
            else
            {
                int kopieerdag = Dagkeuze(menu);
                menu[(keuzedag - 1) * 3 + 1] = menu[(kopieerdag - 1) * 3 + 1];
                menu[(keuzedag - 1) * 3 + 2] = menu[(kopieerdag - 1) * 3 + 2];
            }
            Console.Clear();
            Console.WriteLine(" * Wilt u nog een aanpassing maken?");
            Console.WriteLine("  -------------------------------- ");
            NieuweLijn(1);
            Console.WriteLine(" * 1: Ja");
            NieuweLijn(1);
            Console.WriteLine(" * 2: Nee");
            NieuweLijn(1);
            Console.WriteLine($" * {bevestig}");
            NieuweLijn(1);
            int aanpassing = Keuzestress(1, 2);
            if (aanpassing == 1)
            {
                Aanpassing(menu);
            }
            return menu;
        }
        static int Dagkeuze(string[] menu)
        {
            Console.Clear();
            int count = 3;
            foreach (string x in menu)
            {
                if (count % 3 == 0)
                {
                    Console.Write("  " + count / 3 + " :");
                }
                Console.WriteLine("  " + x);
                Console.WriteLine("  ------------------- ");
                count++;
            }
            NieuweLijn(1);
            Console.WriteLine(" * Kies 1 van de bovenstaande dagen adhv het nummer ");
            NieuweLijn(1);
            Console.WriteLine($" * {bevestig}");
            NieuweLijn(1);
            int dag = Keuzestress(1, (count / 3) - 1);
            return dag;
        }
        static string[] ResetAlles()
        {
            Console.Clear();
            DateTime start = DateTime.Today;
            for (int i = 0; i <= 7; i++)
            {
                Console.WriteLine("  " + (i + 1) + " : " + start.AddDays(i).ToString("dddd, MMMM dd"));
                NieuweLijn(1);
            }
            Console.WriteLine(" * Welke dag wil je beginnen? ");
            Console.WriteLine("  --------------------------- ");
            NieuweLijn(1);
            Console.WriteLine($" * {bevestig}");
            int dagkeuze = Keuzestress(1, 8);
            Console.Clear();
            Console.WriteLine(" * Hoeveel dagen wil je inplannen? (max 14) ");
            Console.WriteLine("  ----------------------------------------- ");
            NieuweLijn(1);
            Console.WriteLine($" * {bevestig}");
            NieuweLijn(1);
            int aantaldagen = Keuzestress(1, 14);
            Console.Clear();
            DateTime today = DateTime.Today.AddDays(dagkeuze - 1);
            int datapunten = aantaldagen * 3;
            string[] menu = new string[datapunten];
            for (int i = 0; i < datapunten; i += 2)
            {
                DateTime day = today;
                menu[i] = day.ToString("dddd, MMMM dd");
                menu[i + 1] = Ingave(" * Wat is de eerste keuze op " + menu[i]);
                NieuweLijn(1);
                menu[i + 2] = Ingave(" * Wat is de tweede keuze op " + menu[i]);
                NieuweLijn(1);
                today = day.AddDays(1);
                Console.Clear();
                i++;
            }
            return menu;
        }
        static int Keuzestress(int lower, int upper)
        {
            int keuze;
            while (!int.TryParse(Console.ReadLine(), out keuze))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(" * Ongeldig antwoord, kies een nummer");
                Console.ForegroundColor = ConsoleColor.Green;
                NieuweLijn(1);
            }
            if (keuze > upper || keuze < lower)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(" * Niet binnen de grenzen");
                Console.ForegroundColor = ConsoleColor.Green;
                NieuweLijn(1);
                keuze = Keuzestress(lower, upper);
            }
            return keuze;
        }
        static string Ingave(string prompt)
        {
            string resultString = string.Empty;
            bool isInPutting = true;
            while (isInPutting)
            {
                Console.WriteLine(prompt);
                resultString = Console.ReadLine();
                resultString = resultString.Trim();
                if (string.IsNullOrEmpty(resultString) == false)
                {
                    isInPutting = false;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(" * U heeft niets ingevoerd. Gelieve uw antwoord in te geven. ");
                    Console.WriteLine();
                    Console.ForegroundColor = ConsoleColor.Green;
                }
            }
            return resultString;
        }
        static void NieuweLijn(int hoeveelLijnen)
        {
            for (int i = 0; i < hoeveelLijnen; i++)
            {
                Console.WriteLine();
            }
        }
    }
}

